# \# Gitlab-runner with own docker image
## follow delow URL for install gitlab-runner
[Install gitlab-runner](https://docs.gitlab.com/runner/install/linux-repository.html)

## register gitlab-runner
```sh
root@b328e6ee74d0:/# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=995 revision=43b2dc3d version=15.4.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/):
[URL from gitlab]
Enter the registration token:
[Token from gitlab]
Enter a description for the runner:
[b328e6ee74d0]: brian
Enter tags for the runner (comma-separated):
brian-test ### 這個tags名稱會需要填到.gitlab-ci.yml 中每個stage 中的tags，否則會跑不起來 ###
Enter optional maintenance note for the runner:
for test
Registering runner... succeeded                     runner=knMCrFCa
Enter an executor: docker, docker-ssh, parallels, shell, ssh, custom, docker+machine, docker-ssh+machine, kubernetes, virtualbox:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
```

## Create .gitlab-ci.yml in the project
```yml
stages:
  - test
  - deploy
test1:
  stage: test
  tags:
    - "brian-test"
  script:
    - echo "Test config1"

test2:
  stage: test
  tags:
    - "brian-test"
  script:
    - echo "Test config2"
    - pwd

deploy1:
  stage: deploy
  tags:
    - "brian-test"
  script:
    - echo "Do your deploy here"
    - ip a

```

# \# Run gitlab runner with official docker image
[official docker runner](https://docs.gitlab.com/runner/install/docker.html)
